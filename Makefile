
NOYAUDIR = .

CC = g++
CFLAGS = -W -Wall -g
# CFLAGS = -W -Wall -g -I./yaml-cpp-yaml-cpp-0.6.3/include
LDFLAGS = -g
# LDFLAGS = -g -L./yaml-cpp-yaml-cpp-0.6.3/build -lyaml-cpp -lX11 -lGL -lpthread -lpng -lstdc++fs -std=c++17
SRC = $(wildcard *.cpp)
OBJ = $(SRC:.cpp=.o)
EXEC = main

all : $(EXEC)

main: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

# main.o: olcPixelGameEngine.h

%.o: %.cpp
	$(CC) -o $@ -c $< $(CFLAGS)

clean :
	rm -f *.o
	rm -f $(TEST)
